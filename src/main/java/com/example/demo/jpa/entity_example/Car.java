package com.example.demo.jpa.entity_example;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
public class Car {
    public Integer getId() {
        return this.id;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String brand;
    @Column
    private String model;
    @Column
    private LocalDate productionDate;
    @Column
    private String color;
    @Column
    private LocalDateTime creationDate;

    public Car(String brand, String model, LocalDate productionDate, String color, LocalDateTime creationDate) {
        this.brand = brand;
        this.model = model;
        this.productionDate = productionDate;
        this.color = color;
        this.creationDate = creationDate;
    }
}
