package com.example.demo.jpa.entity_example;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
public class Person {
    public Person(String firstname, String surname, Integer age, LocalDateTime creationDate) {
        this.firstname = firstname;
        this.surname = surname;
        this.age = age;
        this.creationDate = creationDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false)
    private String firstname;
    @Column(name = "last_name", length = 50, nullable = false)
    private String surname;
    @Column
    private Integer age;
    @Column(nullable = false, updatable = false)
    private LocalDateTime creationDate;
}
