package com.example.demo.jpa.entity_example;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CarDao extends CrudRepository<Car, Integer> {
    List<Car> findByBrand(String brand);
    List<Car> findByModelAndColor(String model, String color);
    List<Car> findAll();
    List<Car> deleteAllByBrandAndModel(String brand, String model);
    List<Car> findByProductionDate(LocalDate productionDate);
    List<Car> findAllByProductionDateBefore(LocalDate productionDate);
    boolean existsByBrandAndModel(String brand, String model);

}
