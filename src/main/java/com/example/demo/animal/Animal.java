package com.example.demo.animal;

public interface Animal {
    String voice();
}
