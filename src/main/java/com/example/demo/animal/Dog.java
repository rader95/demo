package com.example.demo.animal;

import org.springframework.stereotype.Component;

@Component
public class Dog implements Animal{
    @Override
    public String voice() {
        return "Woof woof!";
    }
}
