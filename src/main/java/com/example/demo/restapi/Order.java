package com.example.demo.restapi;
import lombok.AllArgsConstructor;
import lombok.Getter;
@AllArgsConstructor
@Getter
public class Order {
    int id;
    String name;
}
