package com.example.demo.flore;

import org.springframework.stereotype.Component;

@Component
public class Flower {
    public String getName() {
        return "flower";
    }
}
