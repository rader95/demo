package com.example.demo.song;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class SongConfiguration {
    @Bean
    @Scope ("singleton")
    public Song loseYourself() {
        return new Song("Eminem", "Lose Yourself");
    }
    @Bean
    @Scope ("prototype")
    public Song nothingElseMatters() {
        return new Song("Metallica", "Nothing Else Matters");
    }
}
