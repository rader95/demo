package com.example.demo.animal;

import com.example.demo.TestApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import static org.assertj.core.api.Assertions.assertThat;
@SpringBootTest
public class CowTest {
    @Test
    void cowTest() {
        //given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestApplication.class);
        //when
        Cow cow = context.getBean(Cow.class);
        //then
        assertThat(cow.voice()).isEqualTo("Mooooooooo!");
    }

}
