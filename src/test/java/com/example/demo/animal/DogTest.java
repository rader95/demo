package com.example.demo.animal;

import com.example.demo.TestApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import static org.assertj.core.api.Assertions.assertThat;
@SpringBootTest
public class DogTest {
    @Test
    void dogTest() {
        //given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestApplication.class);
        //when
        Dog dog = context.getBean(Dog.class);
        //then
        assertThat(dog.voice()).isEqualTo("Woof woof!");
    }

}
