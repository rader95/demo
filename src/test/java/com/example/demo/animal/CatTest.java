package com.example.demo.animal;

import com.example.demo.TestApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import static org.assertj.core.api.Assertions.assertThat;
@SpringBootTest
public class CatTest {
    @Test
    void catTest() {
        //given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestApplication.class);
        //when
        Cat cat = context.getBean(Cat.class);
        //then
        assertThat(cat.voice()).isEqualTo("Meow!");
    }

}
