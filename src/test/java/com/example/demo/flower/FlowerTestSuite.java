package com.example.demo.flower;

import com.example.demo.TestApplication;
import com.example.demo.flore.Flower;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import static org.assertj.core.api.Assertions.assertThat;
@SpringBootTest
public class FlowerTestSuite {
    @Test
    void flowerBeanTest() {
        //given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestApplication.class);
        //when
        Flower flower = context.getBean(Flower.class);
        //then
        assertThat(flower.getName()).isEqualTo("flower");
    }
}
