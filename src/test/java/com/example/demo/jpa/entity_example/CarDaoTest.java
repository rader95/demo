package com.example.demo.jpa.entity_example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
class CarDaoTest {
    @Autowired
    CarDao carDao;
    private List<Car> cars;
    @BeforeEach
    void setup() {
        cars = Arrays.asList(
                new Car("Citroen", "C3", LocalDate.of(2013,12,23), "white", LocalDateTime.now()),
                new Car("Audi", "A3", LocalDate.of(2008,12,23), "white", LocalDateTime.now()),
                new Car("Citroen", "C4", LocalDate.of(2003,12,23), "black", LocalDateTime.now()),
                new Car("Citroen", "C4", LocalDate.of(2012,12,23), "black", LocalDateTime.now()),
                new Car("BMW", "X3", LocalDate.of(2015,12,23), "white", LocalDateTime.now())
        );
    }
    @Test
    void shouldAddCars() {
        //given
        this.carDao.saveAll(cars);
        //then
        assertThat(cars.size()).isEqualTo(5);

    }
    @Test
    void shouldRetrieveCarByBrand() {
        //given
        this.carDao.saveAll(cars);
        //when
        List<Car> result = this.carDao.findByBrand("Citroen");
        //then
        assertThat(result).hasSize(3);
    }
    @Test
    void shouldRetrieveCarByModelAndColor() {
        //given
        this.carDao.saveAll(cars);
        //when
        List<Car> result = this.carDao.findByModelAndColor("C4","black");
        //then
        assertThat(result).hasSize(2);
    }
    @Test
    void shouldDeleteCarByBrandAndModel() {
        //given
        this.carDao.saveAll(cars);
        //when
        List<Car> result = this.carDao.deleteAllByBrandAndModel("Citroen","C4");
        //then
        List<Car> result2 = this.carDao.findAll();
        assertThat(result).hasSize(2);
        assertThat(result2).hasSize(3);
    }
    @Test
    void shouldRetrieveCarsByProductionDateBefore() {
        //given
        this.carDao.saveAll(cars);
        //when
        List<Car> result = this.carDao.findAllByProductionDateBefore(LocalDate.of(2014,1,1));
        //then
        assertThat(result).hasSize(4);
    }
    @Test
    void shouldReturnTrueIfCarByIdExists() {
        //given
        this.carDao.saveAll(cars);
        //when
        Integer firstCarId = cars.get(0).getId();
        boolean result = this.carDao.existsById(firstCarId);
        //then
        assertThat(result).isTrue();
    }
    @Test
    void shouldReturnTrueIfCarByBrandAndModelExists() {
        //given
        this.carDao.saveAll(cars);
        //when
        boolean result = this.carDao.existsByBrandAndModel("BMW","X3");
        //then
        assertThat(result).isTrue();
    }
    @Test
    void shouldReturnFalseIfCarByBrandAndModelDoesntExist() {
        //given
        this.carDao.saveAll(cars);
        //when
        boolean result = this.carDao.existsByBrandAndModel("BMW","X4");
        //then
        assertThat(result).isFalse();
    }
}