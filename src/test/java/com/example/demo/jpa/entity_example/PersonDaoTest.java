package com.example.demo.jpa.entity_example;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
class PersonDaoTest {
    @Autowired
    PersonDao personDao;
    @Test
    void shouldAddPerson() {
        //given
        Person person = new Person("Radoslaw", "Deresz", 26, LocalDateTime.now());
        //when
        this.personDao.save(person);
        //then
    }
}