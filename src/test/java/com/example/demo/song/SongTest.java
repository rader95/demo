package com.example.demo.song;

import com.example.demo.TestApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SongTest {
    @Test
    void songBeanTest() {
        //given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestApplication.class);
        //when
        Song song = (Song) context.getBean("loseYourself");
        //then
        assertThat(song.getTitle()).isEqualTo("Lose Yourself");
    }
    @Test
    void songSingletonTest() {
        //given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestApplication.class);
        //when
        Song song = (Song) context.getBean("loseYourself");
        song.setTitle("Find Yourself");
        song = (Song) context.getBean("loseYourself");
        //then
        assertThat(song.getTitle()).isEqualTo("Find Yourself");
    }
    @Test
    void songPrototypeTest() {
        //given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestApplication.class);
        //when
        Song song = (Song) context.getBean("nothingElseMatters");
        song.setTitle("Everything Matters");
        song = (Song) context.getBean("nothingElseMatters");
        //then
        assertThat(song.getTitle()).isEqualTo("Nothing Else Matters");
    }
}
