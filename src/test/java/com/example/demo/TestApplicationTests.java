package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
@SpringBootTest
class TestApplicationTests {

	@Test
	void contextLoads() {
	}
	@Test
	void printBeans() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestApplication.class);
		System.out.println("[BEANS]");
		for (String beanDefinitionName : context.getBeanDefinitionNames()) {
			System.out.println(beanDefinitionName);
		}
		System.out.println("[END OF BEANS]");
	}
}
